﻿using NUnit.Framework;
using PaymentProcessor.Logic;
using PaymentProcessor.Models;
using System.Linq;

namespace PaymentProcessor.Tests
{
    [TestFixture]
    public class PremiumPaymentGatewayProcessorRetryTests
    {
        private readonly PaymentBindingModel _overFiveHundred = new PaymentBindingModel {Amount = 501};
        private IPaymentGatewayProcessor _paymentProcessor;

        [Test]
        public void IfServiceValidReturnValidResult()
        {
            _paymentProcessor = new PaymentGatewayProcessor();
            var result = _paymentProcessor.Process(_overFiveHundred);

            Assert.IsTrue(result.IsSuccessful);
            CollectionAssert.IsNotEmpty(result.ResultLog);
            Assert.AreEqual(1, result.ResultLog.Count);
            Assert.IsTrue(typeof(IPremiumPaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
            Assert.IsTrue(result.ResultLog.First().Success);
        }

        [Test]
        public void IfServiceInvalidReturnInvalidResult()
        {
            _paymentProcessor = new PaymentGatewayProcessor {IsServiceValid = false};
            var result = _paymentProcessor.Process(_overFiveHundred);

            Assert.IsFalse(result.IsSuccessful);
            CollectionAssert.IsNotEmpty(result.ResultLog);
            Assert.AreEqual(3, result.ResultLog.Count);
            Assert.IsTrue(typeof(IPremiumPaymentGateway).IsAssignableFrom(result.ResultLog[0].GatewayType));
            Assert.IsFalse(result.ResultLog[0].Success);
            Assert.IsTrue(typeof(IPremiumPaymentGateway).IsAssignableFrom(result.ResultLog[1].GatewayType));
            Assert.IsFalse(result.ResultLog[1].Success);
            Assert.IsTrue(typeof(IPremiumPaymentGateway).IsAssignableFrom(result.ResultLog[2].GatewayType));
            Assert.IsFalse(result.ResultLog[2].Success);
        }
    }
}