﻿using System;
using FluentValidation.TestHelper;
using NUnit.Framework;
using PaymentProcessor.Models.Validation;

namespace PaymentProcessor.Tests
{
    [TestFixture]
    public class PaymentValidatorTests
    {
        private PaymentValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new PaymentValidator();
        }

        [Test]
        public void CreditCardNumberNotSet()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.CreditCardNumber, null as string);
        }

        [Test]
        public void CreditCardNumberNotValid()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.CreditCardNumber, "test");
        }

        [Test]
        public void CreditCardNumberValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.CreditCardNumber, "4532746341119706");
        }

        [Test]
        public void CardHolderNotSet()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.CardHolder, null as string);
        }

        [Test]
        public void CardHolderValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.CardHolder, "Test Test");
        }

        [Test]
        public void ExpirationDateNotSet()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.ExpirationDate, null as DateTime?);
        }

        [Test]
        public void ExpirationDateNotValid()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.ExpirationDate, DateTime.Today.AddDays(-1));
        }

        [Test]
        public void ExpirationDateValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.ExpirationDate, DateTime.Now.AddHours(1));
        }

        [Test]
        public void AmountNotSet()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.Amount, null as decimal?);
        }

        [Test]
        public void AmountNotValid()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.Amount, -1);
        }

        [Test]
        public void AmountValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.Amount, 1);
        }

        [Test]
        public void SecurityCodeNotSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.SecurityCode, null as string);
        }

        [Test]
        public void SecurityCodeLetters()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.SecurityCode, "ABC");
        }

        [Test]
        public void SecurityCodeInvalidLength()
        {
            _validator.ShouldHaveValidationErrorFor(payment => payment.SecurityCode, "12312");
            _validator.ShouldHaveValidationErrorFor(payment => payment.SecurityCode, "12");
        }

        [Test]
        public void SecurityValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(payment => payment.SecurityCode, "555");
        }
    }
}