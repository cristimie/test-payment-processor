﻿using System.Linq;
using NUnit.Framework;
using PaymentProcessor.Logic;
using PaymentProcessor.Models;

namespace PaymentProcessor.Tests
{
    [TestFixture]
    public class PaymentGatewayProcessorInitialTests
    {
        private readonly PaymentBindingModel _underTwenty = new PaymentBindingModel {Amount = 19};
        private readonly PaymentBindingModel _overTwenty = new PaymentBindingModel {Amount = 21};
        private readonly PaymentBindingModel _overFiveHundred = new PaymentBindingModel {Amount = 501};
        private IPaymentGatewayProcessor _paymentProcessor;

        [SetUp]
        public void Setup()
        {
            _paymentProcessor = new PaymentGatewayProcessor();
        }

        [Test]
        public void IfLessThanTwentyProcessCheap()
        {
            var result = _paymentProcessor.Process(_underTwenty);
            Assert.IsTrue(typeof(ICheapPaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
        }

        [Test]
        public void IfBetween21And500ProcessExpensive()
        {
            var result = _paymentProcessor.Process(_overTwenty);
            Assert.IsTrue(typeof(IExpensivePaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
        }

        [Test]
        public void IfOver500ReturnPremium()
        {
            var result = _paymentProcessor.Process(_overFiveHundred);
            Assert.IsTrue(typeof(IPremiumPaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
        }
    }
}