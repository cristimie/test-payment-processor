﻿using System.Linq;
using NUnit.Framework;
using PaymentProcessor.Logic;
using PaymentProcessor.Models;

namespace PaymentProcessor.Tests
{
    [TestFixture]
    public class ExpensivePaymentGatewayProcessorRetryTests
    {
        private readonly PaymentBindingModel _overTwenty = new PaymentBindingModel {Amount = 21};
        private IPaymentGatewayProcessor _paymentProcessor;

        [Test]
        public void IfServiceValidReturnValidResult()
        {
            _paymentProcessor = new PaymentGatewayProcessor();
            var result = _paymentProcessor.Process(_overTwenty);

            Assert.IsTrue(result.IsSuccessful);
            CollectionAssert.IsNotEmpty(result.ResultLog);
            Assert.AreEqual(1, result.ResultLog.Count);
            Assert.IsTrue(typeof(IExpensivePaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
            Assert.IsTrue(result.ResultLog.First().Success);
        }

        [Test]
        public void IfServiceInvalidReturnInvalidResult()
        {
            _paymentProcessor = new PaymentGatewayProcessor{IsServiceValid = false};
            var result = _paymentProcessor.Process(_overTwenty);

            Assert.IsFalse(result.IsSuccessful);
            CollectionAssert.IsNotEmpty(result.ResultLog);
            Assert.AreEqual(2, result.ResultLog.Count);
            Assert.IsTrue(typeof(IExpensivePaymentGateway).IsAssignableFrom(result.ResultLog.First().GatewayType));
            Assert.IsFalse(result.ResultLog.First().Success);
            Assert.IsTrue(typeof(ICheapPaymentGateway).IsAssignableFrom(result.ResultLog.Last().GatewayType));
            Assert.IsFalse(result.ResultLog.Last().Success);
        }
    }
}