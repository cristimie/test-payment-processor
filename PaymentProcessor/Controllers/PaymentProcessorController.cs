﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentProcessor.Logic;
using PaymentProcessor.Models;

namespace PaymentProcessor.Controllers
{
    [Route("ProcessPayment")]
    [ApiController]
    public class PaymentProcessorController : ControllerBase
    {
        private readonly IPaymentGatewayProcessor _paymentGatewayProcessor;

        public PaymentProcessorController(IPaymentGatewayProcessor processor)
        {
            _paymentGatewayProcessor = processor;
        }

        [HttpPost]
        public ProcessedPaymentResult Post([FromBody] PaymentBindingModel value)
        {
            return _paymentGatewayProcessor.Process(value);
        }
    }
}
