﻿using System;
using System.Collections.Generic;

namespace PaymentProcessor.Models
{
    public class ProcessedPaymentResult
    {
        public bool IsSuccessful { get; set; }
        public List<GatewayResult> ResultLog { get; set; } = new List<GatewayResult>();
    }

    public class GatewayResult
    {
        public Type GatewayType { get; set; }
        public bool Success { get; set; }
    }
}
