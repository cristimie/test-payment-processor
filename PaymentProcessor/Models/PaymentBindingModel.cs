﻿using System;

namespace PaymentProcessor.Models
{
    public sealed class PaymentBindingModel
    {
        public string CreditCardNumber { get; set; }
        public string CardHolder { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public decimal? Amount { get; set; }
    }
}
