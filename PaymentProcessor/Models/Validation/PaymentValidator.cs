﻿using System;
using FluentValidation;

namespace PaymentProcessor.Models.Validation
{
    public class PaymentValidator : AbstractValidator<PaymentBindingModel>
    {
        public PaymentValidator()
        {
            RuleFor(p => p.CreditCardNumber).NotEmpty().WithMessage("Credit card number is mandatory");
            RuleFor(p => p.CardHolder).NotEmpty().WithMessage("Card holder is mandatory");
            RuleFor(p => p.ExpirationDate).NotNull().WithMessage("Expiration date is mandatory");
            RuleFor(p => p.Amount).NotNull().WithMessage("Amount is mandatory");

            RuleFor(p => p.Amount).GreaterThan(0).WithMessage("Amount has to be greater than 0");
            RuleFor(p => p.ExpirationDate).GreaterThanOrEqualTo(DateTime.Now).WithMessage("Expiration date cannot be in the past");

            RuleFor(p => p.SecurityCode).Must((o, list, context) =>
            {
                if (string.IsNullOrWhiteSpace(o.SecurityCode)) return true;

                if (o.SecurityCode.Length != 3) return false;
                return IsDigitsOnly(o.SecurityCode);
            }).WithMessage("Security code not valid");

            RuleFor(p => p.CreditCardNumber).CreditCard().WithMessage("Credit card number has to be a certain format");
        }

        bool IsDigitsOnly(string value)
        {
            foreach (char c in value)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}
