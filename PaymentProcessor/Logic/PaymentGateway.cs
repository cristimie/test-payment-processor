﻿namespace PaymentProcessor.Logic
{
    internal abstract class PaymentGateway
    {
        internal bool IsServiceValid { get; set; } = true;
    }
}