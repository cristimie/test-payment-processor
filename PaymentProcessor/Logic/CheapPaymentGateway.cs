﻿using PaymentProcessor.Models;

namespace PaymentProcessor.Logic
{
    internal class CheapPaymentGateway : PaymentGateway, ICheapPaymentGateway
    {
        public bool Process(PaymentBindingModel payment)
        {
            return IsServiceValid;
        }
    }
}
