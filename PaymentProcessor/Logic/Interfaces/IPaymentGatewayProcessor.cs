﻿using PaymentProcessor.Models;

namespace PaymentProcessor.Logic
{
    public interface IPaymentGatewayProcessor
    {
        ProcessedPaymentResult Process(PaymentBindingModel payment);
    }
}
