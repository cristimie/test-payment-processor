﻿using PaymentProcessor.Models;

namespace PaymentProcessor.Logic
{
    public interface IPaymentGateway
    {
        bool Process(PaymentBindingModel payment);
    }
}
