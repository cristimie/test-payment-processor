﻿using System.Linq;
using PaymentProcessor.Models;

namespace PaymentProcessor.Logic
{
    internal class ExpensivePaymentGateway : PaymentGateway, IExpensivePaymentGateway
    {
        public bool Process(PaymentBindingModel payment)
        {
            return IsServiceValid;
        }
    }
}