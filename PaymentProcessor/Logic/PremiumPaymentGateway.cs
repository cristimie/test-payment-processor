﻿using PaymentProcessor.Models;

namespace PaymentProcessor.Logic
{
    internal class PremiumPaymentGateway : PaymentGateway, IPremiumPaymentGateway
    {
        public bool Process(PaymentBindingModel payment)
        {
            return IsServiceValid;
        }
    }
}