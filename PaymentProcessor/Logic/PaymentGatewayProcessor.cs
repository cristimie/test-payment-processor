﻿using PaymentProcessor.Models;
using System;

namespace PaymentProcessor.Logic
{
    public class PaymentGatewayProcessor : IPaymentGatewayProcessor
    {
        private readonly ProcessedPaymentResult _result = new ProcessedPaymentResult();
        private int _currentRetryCount;

        public bool IsServiceValid { get; set; } = true;

        public ProcessedPaymentResult Process(PaymentBindingModel payment)
        {
            var initialGateway = GetInitialGateway(payment);
            var initialResult = initialGateway.Process(payment);
            _result.IsSuccessful = initialResult;
            _result.ResultLog.Add(new GatewayResult { GatewayType = initialGateway.GetType(), Success = initialResult });

            while (!_result.IsSuccessful && _currentRetryCount < GetRetryCount(payment))
            {
                var retryGateway = GetRetryGateway(payment);
                var retryResult = retryGateway.Process(payment);
                _result.IsSuccessful = retryResult;
                _result.ResultLog.Add(new GatewayResult {GatewayType = retryGateway.GetType(), Success = retryResult});
                _currentRetryCount++;
            }

            return _result;
        }

        private IPaymentGateway GetInitialGateway(PaymentBindingModel payment)
        {
            if (payment?.Amount == null || payment.Amount < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (payment.Amount < 20)
            {
                return new CheapPaymentGateway { IsServiceValid = IsServiceValid };
            }

            if (payment.Amount > 500)
            {
                return new PremiumPaymentGateway { IsServiceValid = IsServiceValid };
            }

            return new ExpensivePaymentGateway { IsServiceValid = IsServiceValid };
        }

        private IPaymentGateway GetRetryGateway(PaymentBindingModel payment)
        {
            return payment.Amount > 500
                ? (IPaymentGateway)new PremiumPaymentGateway { IsServiceValid = IsServiceValid }
                : new CheapPaymentGateway { IsServiceValid = IsServiceValid };
        }

        private int GetRetryCount(PaymentBindingModel payment)
        {
            return payment.Amount < 20 ? 0 : payment.Amount > 500 ? 2 : 1;
        }
    }
}
